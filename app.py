from flask import Flask, render_template, request, jsonify
import asyncio
import requests
import requests_cache

requests_cache.install_cache(
    cache_name='hotel_cache', backend='sqlite', expire_after=180)
loop = asyncio.new_event_loop()
app = Flask(__name__)


async def get_data(city, checkin, checkout):
    snaptravel_data = {
        'city': city,
        'checkin': checkin,
        'checkout': checkout,
        'provider': 'snaptravel'
    }

    retail_data = {
        'city': city,
        'checkin': checkin,
        'checkout': checkout,
        'provider': 'snaptravel'
    }

    url = 'https://experimentation.snaptravel.com/interview/hotels'

    snaptravel_future = loop.run_in_executor(
        None, requests.post, url, None, snaptravel_data)
    retail_future = loop.run_in_executor(
        None, requests.post, url, None, retail_data)

    snaptravel_response = await snaptravel_future
    retail_response = await retail_future
    snaptravel_content = snaptravel_response.json()
    retail_content = retail_response.json()
    # print(snaptravel_content)
    # print(retail_content)
    return [snaptravel_content, retail_content]


@app.route('/', methods=['POST', 'GET'])
def demo():
    if request.method == 'POST':
        city_string_input = request.form['city']
        checkin_string_input = request.form['checkin']
        checkout_string_input = request.form['checkout']

        hotel_data = loop.run_until_complete(get_data(city_string_input, checkin_string_input,
                                                      checkout_string_input))
        snaptravel_hotels = hotel_data[0]['hotels']
        retail_hotels = hotel_data[1]['hotels']
        print(len(snaptravel_hotels))
        print(len(retail_hotels))
        ids = {}
        data = []
        for hotel in snaptravel_hotels:
            ids[hotel['id']] = hotel

        for hotel in retail_hotels:
            if hotel['id'] in ids:
                ids[hotel['id']]['retail_price'] = hotel['price']
                data.append(ids[hotel['id']])

        print("data\n", data)

        return render_template('demo.html', data=data)
    return render_template('form.html')
